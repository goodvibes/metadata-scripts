Here are a few scripts to fetch metadata from my favorite radio stations. By
"metadata" I mean the information regarding the current track: title, artist
and possibly more.

This is useful when I want to know quickly what song is playing now, it's much
better to type a quick command in the terminal rather than opening the radio
website in the web browser.

Of course, if only radios were embedding the metadata in the audio stream,
there would be no need for this kind of scripts.

Install the scripts with:

    sudo install *-meta /usr/local/bin

Be sure to install the following tools as well:

- [jq](https://stedolan.github.io/jq) - Command-line JSON processor
- [pup](https://github.com/ericchiang/pup) - Command-line HTML processor
- [recode](https://github.com/rrthomas/recode) - Character set conversion tool

    sudo apt-get install jq pup recode
